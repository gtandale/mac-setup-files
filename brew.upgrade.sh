#!/usr/bin/env bash

# Is there anything lingering
brew cleanup

# Tap third-party formulae
brew tap homebrew/cask-versions

# Update Repositories
brew update

# Upgrade Packages
brew upgrade