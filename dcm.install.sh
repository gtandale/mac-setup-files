#!/usr/bin/env bash

SETUP_FOLDER=$(pwd)

# installs Beanworks software stack at ~/beanworks
cd ~
[ -d beanworks ] || mkdir beanworks && cd beanworks
[ -d dcm ] || git clone https://github.com/beanworks/dcm.git
cd dcm

# installs a 'Mac-friendly' bean.yml in ~/beanworks/dcm
if  [ ! -d beanyml ]; then
    if ! (git clone git@bitbucket.org:beanworks/beanyml.git) then
        echo "No permission to clone beanyml, please check your ssh-key" 
        exit    
    fi
fi
cd beanyml && \
git checkout master && \
./setup.sh

# source beanworks config

echo "Update your shell profile (eg. .zshrc) with 'source  $SETUP_FOLDER/beanworks.include.sh'"
echo "Now you can run dcm setup"
